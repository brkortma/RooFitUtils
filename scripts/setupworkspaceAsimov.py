import ROOT
from ROOT import RooCategory

def createAsimov(ws,mc,asmName):
    import ROOT
    allParams = ROOT.RooArgSet()
    allParams.add(mc.GetGlobalObservables())
    allParams.add(mc.GetObservables())
    allParams.add(mc.GetNuisanceParameters())
    allParams.add(mc.GetParametersOfInterest())
    globs = mc.GetGlobalObservables().snapshot()
    asimovData = ROOT.RooStats.AsymptoticCalculator.MakeAsimovData(mc,allParams,globs)
    asimovData.SetName(asmName)
    getattr(ws,"import")(asimovData)



def main(args):
    inwsfile = ROOT.TFile.Open(args.workspace[0],"READ")
    if not inwsfile or not inwsfile.IsOpen():
        raise RuntimeError("unable to open file '{:s}'".format(args.workspace[0]))
    workspace = inwsfile.Get(args.workspace[1])
    workspace.Print("t")






    #### Get the old ModelConfig
    mcold = workspace.obj("WWSMEFT_model")
    #####
    allvariables = workspace.allVars()
    pois = mcold.GetParametersOfInterest()
    nuis = mcold.GetNuisanceParameters()
    globs = mcold.GetGlobalObservables()
    obs = mcold.GetObservables()

    #### Get the pdf
    pdf = workspace.obj("WWSMEFT_pdf")


    ##### Make the new workspace and ModelConfig
    newws = ROOT.RooWorkspace("SMWWasimov")
    mcnew = ROOT.RooStats.ModelConfig("ModelConfig",newws)
    getattr(newws,'import')(mcnew)

    #### get the data
    data = workspace.data("WWSMEFT_data")
    data.Print("v")

    sample = ROOT.RooCategory("physics", "physics")
    sample.defineType("SMtoWW")
    combData = ROOT.RooDataSet("AsimovData", "AsimovData",obs,ROOT.RooFit.Index(sample),ROOT.RooFit.Import("SMtoWW", data))
    simPdf = ROOT.RooSimultaneous("simPdf", "simultaneous pdf", sample)
    simPdf.addPdf(pdf,"SMtoWW")
    getattr(newws,'import')(allvariables)
    getattr(newws,'import')(combData)
    getattr(newws,'import')(simPdf)

    mcnew.SetParametersOfInterest(pois)
    mcnew.SetPdf(newws.obj("simPdf"))
    mcnew.SetObservables(obs)
    empty = ROOT.RooArgSet()
    mcnew.SetNuisanceParameters(nuis)
    mcnew.SetGlobalObservables(empty)
    mcnew.Print()
    mcnew.SetWS(newws)
    getattr(newws,'import')(mcnew,True)
    newws.saveSnapshot("SnSh_AllVars_Nominal",allvariables)
    newws.writeToFile(args.output)
    newws.Print("t")
    newws.obj("ModelConfig").Print("t")

    #
    # iter = allvariables.createIterator()
    # var = iter.Next()
    # other = ["ww_ewk","ww_pdf","ww_scale","ww_mcstat1","ww_mcstat2","ww_norm"]
    # while var:
    #     print var.GetName()
    #     if "_meas_" in var.GetName():
    #         x = workspace.obj(var.GetName())
    #         x.setConstant(False)
    #         x.setRange(0,100)
    #         obs.add(x)
    #     if "mcstat_" in var.GetName():
    #         x = workspace.obj(var.GetName())
    #         mcstat.add(x)
    #     if var.GetName().startswith("ww_"):
    #         x = workspace.obj(var.GetName())
    #         mcstat.add(x)
    #         print("added " + var.GetName())
    #     var = iter.Next()
    # iter = poi.createIterator()
    # var = iter.Next()
    #
    # while var:
    #     var.setConstant(True)
    #     var.setVal(0.0)
    #     var = iter.Next()
    #
    # poidata = ROOT.RooDataSet("poidata","poidata",poi)
    #
    # newwsasimov = ROOT.RooWorkspace("SMWW_asimov")
    # mcasimov = ROOT.RooStats.ModelConfig("ModelConfig",newwsasimov)
    # getattr(newwsasimov,'import')(allvariables,ROOT.RooFit.RecycleConflictNodes())
    # getattr(newwsasimov,'import')(pdf,ROOT.RooFit.RecycleConflictNodes())
    # getattr(newwsasimov,'import')(poidata,ROOT.RooFit.RecycleConflictNodes())
    #
    #
    # nll = newwsasimov.obj("pdf").createNLL(poidata)
    # Minimizer = ROOT.RooMinimizer(nll)
    # Minimizer.minimize("Minuit2","Migrad")
    # Minimizer.migrad()
    #
    #
    # result = Minimizer.save()
    # result.Print()
    #
    # asimovData = ROOT.RooDataSet("asimovData", "asimovData",obs,ROOT.RooFit.Index(sample),ROOT.RooFit.Import("SMtoWW", data))




    # newwsasimov = ROOT.RooWorkspace("SMWW_asimov")
    # mcasimov = ROOT.RooStats.ModelConfig("ModelConfig",newwsasimov)
    # getattr(newwsasimov,'import')(allvariables,ROOT.RooFit.RecycleConflictNodes())
    # getattr(newwsasimov,'import')(simPdf,ROOT.RooFit.RecycleConflictNodes())
    # getattr(newwsasimov,'import')(combData,ROOT.RooFit.RecycleConflictNodes())

    #
    #
    # mcasimov.SetParametersOfInterest(obs)
    # mcasimov.SetPdf(newws.obj("simPdf"))
    # mcasimov.SetObservables(poi)
    # empty = ROOT.RooArgSet()
    # mcasimov.SetNuisanceParameters(mcstat)
    # mcasimov.SetGlobalObservables(empty)
    # mcasimov.SetSnapshot(allvariables)
    # mcasimov.Print()
    # poidata = ROOT.RooDataSet("poidata","poidata",poi)


if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="script to inject STXS morphing function into a workspace")
    parser.add_argument("--workspace",help="input workspace file and object name",type=str,nargs=2,required=True)
    parser.add_argument("--output",help="input workspace file and object name",type=str,default="setup_SMWW_workspace_mu_ww_Asimov.root")
    args = parser.parse_args()
    main(args)
