from ROOT import RooFormulaVar, RooRealVar
from os.path import join as pjoin
def createVBFMorphingFunction(name,infilename,histname):
    import ROOT
    from ROOT import TFile
    roottemp = TFile(infilename)
    roottemp.cd()
    inputs = ROOT.RooArgList()
    inputnames = []
    for k in ROOT.gDirectory.GetListOfKeys():
        sample = k.GetName()
        v = ROOT.RooStringVar(sample,sample,sample)
        inputnames.append(v)
        inputs.add(v)
    #setup morphfunc by hand

    morphfunc = ROOT.RooSMEFTvbfMorphFunc(name,name,infilename,histname,inputs)
    return morphfunc


def opt_merge_max_mappings(dict1, dict2):
    """ Merges two dictionaries based on the largest value in a given mapping.

    Parameters
    ----------
    dict1 : Dict[Any, Comparable]
    dict2 : Dict[Any, Comparable]

    Returns
    -------
    Dict[Any, Comparable]
        The merged dictionary
    """
    # we will iterate over `other` to populate `merged`
    merged, other = (dict1, dict2) if len(dict1) > len(dict2) else (dict2, dict1)
    merged = dict(merged)

    for key in other:
        if key not in merged or other[key] > merged[key]:
            merged[key] = other[key]
    return merged

def makeCleanWorkspace(oldWS,newName,mcname,newpdf):
    import ROOT

    #clone a workspace, copying all needed components and discarding all others
    objects = oldWS.allGenericObjects()
    oldMC = oldWS.obj(mcname)
    data = oldWS.allData()

    poilist = oldMC.GetParametersOfInterest()
    nplist = oldMC.GetNuisanceParameters()
    obslist = oldMC.GetObservables()
    globobslist = oldMC.GetGlobalObservables()
    oldpdf = oldWS.pdf("combPdf")

    #Make newWS
    newWS = ROOT.RooWorkspace(oldWS.GetName(),oldWS.GetTitle())
    newWS.autoImportClassCode(True)
    newMC = ROOT.RooStats.ModelConfig("ModelConfig",newWS)


    getattr(newWS,'import')(newpdf)
    newPdf =newWS.pdf(newpdf.GetName())
    newMC.SetPdf(newPdf)



    newnplist=ROOT.RooArgSet()

    iter = nplist.createIterator()
    var = iter.Next()
    while var:
        print var.GetName()
        if "mu_gg2H_" in var.GetName():
            var = iter.Next()
            continue
        if "mu_VBF_HWWRun2VBF" in var.GetName():
            var = iter.Next()
            continue
        if "mu_VBF_qq2Hqq_" in var.GetName():
            var = iter.Next()
            continue
        newnplist.add(var)
        var = iter.Next()



    for d in data:
        getattr(newWS,'import')(d)


    newMC.SetParametersOfInterest(poilist)
    newMC.SetNuisanceParameters(newnplist)
    newMC.SetObservables(obslist)
    newMC.SetGlobalObservables(globobslist)
    getattr(newWS,'import')(newMC)

    return newWS

def main(args):
    import ROOT
    inwsfile = ROOT.TFile.Open(args.workspace[0],"READ")
    if not inwsfile or not inwsfile.IsOpen():
        raise RuntimeError("unable to open file '{:s}'".format(args.workspace[0]))

    workspace = inwsfile.Get(args.workspace[1])
    if not workspace:
        raise RuntimeError("unable to load object '{:s}', available keys are \n".format(args.workspace[1])
                           + "\n".join([ "    '{:s}' ({:s})".format(k.GetName(),k.GetClassName())  for k in inwsfile.GetListOfKeys() ]))


    if args.workspacedata:
        inwsfiledata = ROOT.TFile.Open(args.workspacedata[0],"READ")
        wsextradata = inwsfiledata.Get(args.workspacedata[1])
        data = wsextradata.obj("combData")
        data.SetNameTitle("AsimovData","AsimovData")
        getattr(workspace,'import')(data)

    coefflist_massless=["cHW","clu","clq3","clq1","cld","cW","cHl1","cHq3","cHu","cHd","cHWB","cHB","cHDD","cHG","cHbox","cHl3","cHq1","cll1"]
    for coeff in coefflist_massless:
        workspace.obj(coeff).setRange(-75,75)


    if args.output:
        workspace.writeToFile(args.output)
        print("done writing "+args.output)

    if args.outputdir:
        for item in coefflist_massless:
            workspace.writeToFile(pjoin(pjoin(args.outputdir,"HWWSMWWfull-"+item),"workspace-preFit.root"))



if __name__ == "__main__":
    from argparse import ArgumentParser
    parser = ArgumentParser(description="script to inject STXS morphing function into a workspace")
    parser.add_argument("--workspace",help="input workspace file and object name",type=str,nargs=2,required=True)
    parser.add_argument("--output",help="output workspace",type=str)
    parser.add_argument("--outputdir",help="output workspace",type=str)
    parser.add_argument("--workspacedata",help="input workspace file and object name",type=str,nargs=2,required=True)

    args = parser.parse_args()
    main(args)
