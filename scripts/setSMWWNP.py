import ROOT
from argparse import ArgumentParser
import QFramework
import SFramework

def main(args):
    inwsfile = ROOT.TFile.Open(arguments.workspace[0],"READ")
    if not inwsfile or not inwsfile.IsOpen():
        raise RuntimeError("unable to open file '{:s}'".format(args.workspace[0]))


    w = inwsfile.Get(arguments.workspace[1])
    #w.Print() #print workspace information
    mc = w.obj("ModelConfig")
    mc.Print()
    SMWWpdf = w.pdf("WWSMEFT_pdf_complete_edit_complete_SMWW")
    SMWWpdf.Print("t")
    args = w.allVars()
    NP_edit_SMWW = []
    iter = args.createIterator()
    var = iter.Next()
    while var:
        print var.GetName()
        if ("_edit_SMWW" in var.GetName()):
            NP_edit_SMWW.append(var.GetName())
        var = iter.Next()
    oldNP = mc.GetNuisanceParameters()
    oldNP.Print()
    iteroldNP = oldNP.createIterator()
    varoldNP = iteroldNP.Next()
    coeff = ["cHW","clu","clq3","clq1","cld","cW","cHl1","cHq3","cHu","cHd","cHWB","cHB","cHDD","cHG","cHbox","cHl3","cHq1","cll1"]
    while varoldNP:

        if varoldNP.GetName() in coeff:
            varoldNP = iteroldNP.Next()
            continue
        print varoldNP.GetName()
        NP_edit_SMWW.append(varoldNP.GetName())
        varoldNP = iteroldNP.Next()

    mc.SetNuisanceParameters(",".join(NP_edit_SMWW))



    for par in NP_edit_SMWW:
        if not "gamma" in par:
            w.obj(par).setRange(-5,5)
        w.obj(par).Print()
        if "edit_SMWW" in par:
            parameter = w.obj(par)
            parameter.SetName(par.replace("edit_SMWW","SMWW"))
            continue
        if "auto_auto_" in par:
            parameter = w.obj(par)
            parameter.SetName(par.replace("_auto_auto_",""))
            continue
        if "auto_" in par:
            parameter = w.obj(par)
            parameter.SetName(par.replace("auto_",""))
            continue
        if not "HWW" in par:
            parameter = w.obj(par)
            parameter.SetName(par+"_HWWSMWW")
            continue

    GaussianSet = {}
    string = ""
    with open(arguments.correlations, "r") as file:
        Gaussian = 0
        for line in file:
            stripped_line = line.strip()
            if line == '\n':
                continue
            if "Gaussian" in line:
                Gaussian = 1
                continue
            if Gaussian==0:
                continue
            if Gaussian == 1 and "####" in line:
                break
            if "SMWW::" in line:
                GausianConstraint = stripped_line.split(">>")[1]
                string = string + ",constraint_" + GausianConstraint+ "_edit_SMWW="+GausianConstraint+"Constraint"

    print(string)
    edit = "EDIT::combPdf(combPdf" + string +")"
    print(edit)
    newpdf = w.factory(edit)
    raw_input()


    newpdf.Print("t")

    ##### Make the new workspace and ModelConfig
    newws = ROOT.RooWorkspace("combined")
    mcnew = ROOT.RooStats.ModelConfig("ModelConfig",newws)
    getattr(newws,'import')(mcnew)
    getattr(newws,'import')(newpdf)
    #### get the data
    data = w.data("combData")
    getattr(newws,'import')(data)

    pois = mc.GetParametersOfInterest()
    nuis = mc.GetNuisanceParameters()
    globs = mc.GetGlobalObservables()
    obs = mc.GetObservables()

    mcnew.SetParametersOfInterest(pois)
    mcnew.SetPdf(newws.obj("combPdf"))
    mcnew.SetObservables(obs)
    mcnew.SetNuisanceParameters(nuis)
    mcnew.SetGlobalObservables(globs)
    mcnew.Print()
    mcnew.SetWS(newws)
    getattr(newws,'import')(mcnew,True)
    newws.obj("JetBJES_HWWSMWW").Print("v")
    newws.obj("ModelConfig").Print()



    newws.writeToFile(arguments.output)





if __name__ == "__main__":
    parser = ArgumentParser(description="view workspaces")
    parser.add_argument("--workspace",help="input workspace file and object name",type=str,nargs=2,required=True)
    parser.add_argument("--output",type=str,required=True)
    parser.add_argument("--correlations",type=str,required=True)
    arguments = parser.parse_args()
    main(arguments)
