#!/bin/env python
import ROOT

def createAsimov(ws,mc,asmName):
    import ROOT
    allParams = ROOT.RooArgSet()
    allParams.add(mc.GetGlobalObservables())
    allParams.add(mc.GetObservables())
    allParams.add(mc.GetNuisanceParameters())
    allParams.add(mc.GetParametersOfInterest())
    globs = mc.GetGlobalObservables().snapshot()
    asimovData = ROOT.RooStats.AsymptoticCalculator.MakeAsimovData(mc,allParams,globs)
    asimovData.SetName(asmName)
    getattr(ws,"import")(asimovData)

def removeSMWWpdf(ws,mc):

    #allVars = ws.allVars()
    mc = ws.obj("ModelConfig")
    #mc.Print()
    simPdf = mc.GetPdf()

    model_CR_0j_DF_Ztt = simPdf.getPdf("CR_0j_DF_Ztt_HWWRun2GGF_HWW")
    model_CR_0j_DF_top = simPdf.getPdf("CR_0j_DF_top_HWWRun2GGF_HWW")
    model_CR_1j_DF_WW= simPdf.getPdf("CR_1j_DF_WW_HWWRun2GGF_HWW")
    model_CR_1j_DF_Ztt= simPdf.getPdf("CR_1j_DF_Ztt_HWWRun2GGF_HWW")
    model_CR_1j_DF_top= simPdf.getPdf("CR_1j_DF_top_HWWRun2GGF_HWW")
    model_SR_0j_DF_Mll1_PtSubLead2_e= simPdf.getPdf("SR_0j_DF_Mll1_PtSubLead2_e_HWWRun2GGF_HWW")
    model_SR_0j_DF_Mll1_PtSubLead2_m= simPdf.getPdf("SR_0j_DF_Mll1_PtSubLead2_m_HWWRun2GGF_HWW")
    model_SR_0j_DF_Mll1_PtSubLead3_e= simPdf.getPdf("SR_0j_DF_Mll1_PtSubLead3_e_HWWRun2GGF_HWW")
    model_SR_0j_DF_Mll1_PtSubLead3_m= simPdf.getPdf("SR_0j_DF_Mll1_PtSubLead3_m_HWWRun2GGF_HWW")
    model_SR_0j_DF_Mll2_PtSubLead2_e= simPdf.getPdf("SR_0j_DF_Mll2_PtSubLead2_e_HWWRun2GGF_HWW")
    model_SR_0j_DF_Mll2_PtSubLead2_m= simPdf.getPdf("SR_0j_DF_Mll2_PtSubLead2_m_HWWRun2GGF_HWW")
    model_SR_0j_DF_Mll2_PtSubLead3_e= simPdf.getPdf("SR_0j_DF_Mll2_PtSubLead3_e_HWWRun2GGF_HWW")
    model_SR_0j_DF_Mll2_PtSubLead3_m= simPdf.getPdf("SR_0j_DF_Mll2_PtSubLead3_m_HWWRun2GGF_HWW")
    model_SR_1j_DF_Mll1_PtSubLead2_e= simPdf.getPdf("SR_1j_DF_Mll1_PtSubLead2_e_HWWRun2GGF_HWW")
    model_SR_1j_DF_Mll1_PtSubLead2_m= simPdf.getPdf("SR_1j_DF_Mll1_PtSubLead2_m_HWWRun2GGF_HWW")
    model_SR_1j_DF_Mll1_PtSubLead3_e= simPdf.getPdf("SR_1j_DF_Mll1_PtSubLead3_e_HWWRun2GGF_HWW")
    model_SR_1j_DF_Mll1_PtSubLead3_m= simPdf.getPdf("SR_1j_DF_Mll1_PtSubLead3_m_HWWRun2GGF_HWW")
    model_SR_1j_DF_Mll2_PtSubLead2_e= simPdf.getPdf("SR_1j_DF_Mll2_PtSubLead2_e_HWWRun2GGF_HWW")
    model_SR_1j_DF_Mll2_PtSubLead2_m= simPdf.getPdf("SR_1j_DF_Mll2_PtSubLead2_m_HWWRun2GGF_HWW")
    model_SR_1j_DF_Mll2_PtSubLead3_e= simPdf.getPdf("SR_1j_DF_Mll2_PtSubLead3_e_HWWRun2GGF_HWW")
    model_SR_1j_DF_Mll2_PtSubLead3_m= simPdf.getPdf("SR_1j_DF_Mll2_PtSubLead3_m_HWWRun2GGF_HWW")

    ##vbf
    model_temme_SR_HWWRun2VBF= simPdf.getPdf("temme_SR_HWWRun2VBF_HWW")
    model_temme_TopCR_HWWRun2VBF= simPdf.getPdf("temme_TopCR_HWWRun2VBF_HWW")
    model_temme_ZtautauCR_HWWRun2VBF= simPdf.getPdf("temme_ZtautauCR_HWWRun2VBF_HWW")

    ##take out this one SMWW
    pdf_complete_SMWWchannel = simPdf.getPdf("WWSMEFT_pdf_complete_SMWW")

    channelCat = ws.cat("master_measurement")#ROOT.RooCategory("channelCat","channelCat")
    #
    simPdfnew = ROOT.RooSimultaneous("combPdf","combPdf",channelCat)

    simPdfnew.addPdf(model_CR_0j_DF_Ztt,"CR_0j_DF_Ztt_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_CR_0j_DF_top,"CR_0j_DF_top_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_CR_1j_DF_WW,"CR_1j_DF_WW_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_CR_1j_DF_Ztt,"CR_1j_DF_Ztt_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_CR_1j_DF_top,"CR_1j_DF_top_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_0j_DF_Mll1_PtSubLead2_e,"SR_0j_DF_Mll1_PtSubLead2_e_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_0j_DF_Mll1_PtSubLead2_m,"SR_0j_DF_Mll1_PtSubLead2_m_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_0j_DF_Mll1_PtSubLead3_e,"SR_0j_DF_Mll1_PtSubLead3_e_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_0j_DF_Mll1_PtSubLead3_m,"SR_0j_DF_Mll1_PtSubLead3_m_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_0j_DF_Mll2_PtSubLead2_e,"SR_0j_DF_Mll2_PtSubLead2_e_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_0j_DF_Mll2_PtSubLead2_m,"SR_0j_DF_Mll2_PtSubLead2_m_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_0j_DF_Mll2_PtSubLead3_e,"SR_0j_DF_Mll2_PtSubLead3_e_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_0j_DF_Mll2_PtSubLead3_m,"SR_0j_DF_Mll2_PtSubLead3_m_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_1j_DF_Mll1_PtSubLead2_e,"SR_1j_DF_Mll1_PtSubLead2_e_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_1j_DF_Mll1_PtSubLead2_m,"SR_1j_DF_Mll1_PtSubLead2_m_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_1j_DF_Mll1_PtSubLead3_e,"SR_1j_DF_Mll1_PtSubLead3_e_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_1j_DF_Mll1_PtSubLead3_m,"SR_1j_DF_Mll1_PtSubLead3_m_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_1j_DF_Mll2_PtSubLead2_e,"SR_1j_DF_Mll2_PtSubLead2_e_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_1j_DF_Mll2_PtSubLead2_m,"SR_1j_DF_Mll2_PtSubLead2_m_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_1j_DF_Mll2_PtSubLead3_e,"SR_1j_DF_Mll2_PtSubLead3_e_HWWRun2GGF_HWW")
    simPdfnew.addPdf(model_SR_1j_DF_Mll2_PtSubLead3_m,"SR_1j_DF_Mll2_PtSubLead3_m_HWWRun2GGF_HWW")

    simPdfnew.addPdf(model_temme_SR_HWWRun2VBF,"temme_SR_HWWRun2VBF_HWW")
    simPdfnew.addPdf(model_temme_TopCR_HWWRun2VBF,"temme_TopCR_HWWRun2VBF_HWW")
    simPdfnew.addPdf(model_temme_ZtautauCR_HWWRun2VBF,"temme_ZtautauCR_HWWRun2VBF_HWW")


    POIS = mc.GetParametersOfInterest()
    NPS = mc.GetNuisanceParameters()
    OBS = mc.GetObservables()
    globalOBS = mc.GetGlobalObservables()

    newws = ROOT.RooWorkspace("combined","combined")
    mcnew = ROOT.RooStats.ModelConfig("ModelConfig","ModelConfig")
    mcnew.SetWS(newws)
    mcnew.SetPdf(simPdfnew)
    mcnew.SetParametersOfInterest(POIS)
    mcnew.SetNuisanceParameters(NPS)
    mcnew.SetObservables(OBS)
    mcnew.SetGlobalObservables(globalOBS)
    #getattr(newws, 'import')(allVars)


    for d in ws.allData():
        getattr(newws, 'import')(d)
    ws.data("combData").Print("v")

    model_CR_0j_DF_Ztt.Print("t")
    ws.obj("obs_x_CR_0j_DF_Ztt").Print("v")
    return


    print("importing now")
    getattr(newws, 'import')(mcnew)
    #x = ROOT.RooFit.RecycleConflictNodes()
    #getattr(newws, 'import')(newpdf)



    #newws.obj("ATLAS_norm_WW_0jet_HWWRun2GGF").setConstant(True)


    return newws



def main(args):
    import gc
    gc.disable()


    infile = ROOT.TFile.Open(args.input)
    if not infile or not infile.IsOpen():
      print("unable to open file "+args.input)
      exit(0)

    ws = infile.Get(args.workspace)
    if not ws:
      print("unable to obtain workspace "+args.workspace)
      exit(0)

    mc = ws.obj(args.mc)
    if not mc:
      print("unable to obtain model config "+args.mc)
      exit(0)

    if ws.data(args.name):
      print("a dataset with the name "+args.name+" is already present in the workspace!")
      exit(0)

    allVars = ws.allVars().snapshot()

    for assignment in args.parameters:
      p,v = assignment.split("=")
      par = allVars.find(p)
      if not par:
        print("unable to find parameter "+p)
        exit(0)
      val = float(v)
      if val > par.getMax():
        par.setMax(val)
      if val < par.getMin():
        par.setMin(val)
      par.setVal(val)


    ws_noSM = removeSMWWpdf(ws,mc)
    return



    createAsimov(ws_noSM,mc,args.name)
    return

    ws.writeToFile(args.output)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser("create an asimov dataset for a workspace",formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("input", help="Input workspace to be used")
    parser.add_argument("--output", default="workspace-output.root", help="Output workspace")
    parser.add_argument("--workspace", default="combined", help="Workspace name")
    parser.add_argument("--name", default="asimovData", help="Dataset name")
    parser.add_argument("--parameters", nargs="+", help="values of parameters to be used", metavar="par=x", default=[])
    parser.add_argument("--mc", default="ModelConfig", help="ModelConfig name")

    args = parser.parse_args()
    main(args)
