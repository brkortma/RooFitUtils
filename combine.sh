#!bin/bash

#Make HWW mu workspace
python share/combine.py --input comb_HWW/combine_noSTXS.cfg --correlations comb_HWW/correlations-noSTXS.txt --output ../../share/workspaces/HWWRun2-nostxs-c16a/workspace-preFit.root --poi "mu_VBF" "mu_GGF"


#Make HWW WWCR mu workspace
python share/combine.py --input comb_HWW/combine_noSTXS_WWCR.cfg --correlations comb_HWW/correlations-noSTXS.txt --output ../../share/workspaces/HWWRun2-nostxs-WWCR-c16a/workspace-preFit.root --poi "mu_VBF" "mu_GGF"



#Make HWW STXS workspace
python share/combine.py --input comb_HWW/combine-stxs-v7.cfg --correlations comb_HWW/correlations-stxs-v7.txt --output ../../share/workspaces/HWWRun2-STXS-c16a/workspace-preFit.root

#
--poi "mu_gg2H_0J" "mu_gg2H_1J_ptH_0_60" "mu_gg2H_1J_ptH_120_200" "mu_gg2H_1J_ptH_60_120" "mu_gg2H_1J_ptH_gt200" "mu_gg2H_VBFtopo_jet3" "mu_gg2H_VBFtopo_jet3veto" "mu_gg2H_fwdH" "mu_gg2H_ge2J_ptH_0_60" "mu_gg2H_ge2J_ptH_120_200" "mu_gg2H_ge2J_ptH_60_120" "mu_gg2H_ge2J_ptH_gt200" "mu_VBF_qq2Hqq_VBFtopo_jet3" "mu_VBF_qq2Hqq_VBFtopo_jet3veto" "mu_VBF_qq2Hqq_VH2jet" "mu_VBF_qq2Hqq_fwdH" "mu_VBF_qq2Hqq_pTjet1_gt200" "mu_VBF_qq2Hqq_rest"

#Make HWW+SMWW mu workspace
python share/combine.py --input comb_SMWW/combine_noSTXS.cfg --correlations comb_SMWW/correlations-noSTXS.txt --output ../../share/workspaces/HWWSMWW-c16a/workspace-preFit.root --poi "mu_GGF" "mu_VBF"

#Make HWW+SMWW asimov mu workspace
python share/combine.py --input comb_SMWW/combine_noSTXSasimov.cfg --correlations comb_SMWW/correlations-noSTXS.txt --output ../../share/workspaces/HWWSMWW-c16a/workspace-preFit-asimov.root --poi "mu_GGF" "mu_VBF"
