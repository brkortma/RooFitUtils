# RooFitUtils

This package is a collection of tools for advanced workspace
manipulation and fitting using RooFit. Among others, it includes

 * ExtendedMinimizer and ExtendedModel, advanced fitting tools originally designed by Stefan Gadatsch
 * editws, workspace manipulation functions designed by Tim Adye.
 * guessCorrelations, a helper script able to intelligently identify nuisance parameters when combining workspaces.

## Setup

This package supports setup within RootCore and CMake based ASG
releases as well as standalone compilation with ROOT.

In order to compile with CMake, type

    mkdir build
    cd build
    cmake .. -DCMAKE_MODULE_PATH=$ROOTSYS/cmake/
    make -j4
    cd ..
    source setup.sh

Now, you are ready to use the package. Don't forget to

    source setup.sh

every time you create a new shell.

It is recommended to have python 2.7 or higher.
scipy and scikit-image packages are required
for plotting likelihood results.

First make sure you have pip installed

    curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
    python get-pip.py

and then install the packages by typing

    pip install --user scipy
    pip install --user scikit-image

## Usage

A fitting font-end are provided in the form of a python script

    scripts/fit.py

which provides extensive help with the `--help` command line option. The results of likelihood scans obtained with this script can be plotted using

    scripts/plotscan.py


## SMWW + HWW combination

First prepare the SMWW workspace for usage with RooFitUtils and the SFramework, Renaming data, ModelConfig. This can be done by running it through the combiner which will
put everything in the right place.

    python scripts/setupworkspace.py --workspace ../../input/original/SMWW/SMWW_workspace_mu_ww_noEFT.root w  --output ../../input/edited/SMWW/setup_SMWW_workspace_mu_ww_Observed.root

Do the Same for the Asimov only workspace

    python scripts/setupworkspaceAsimov.py --workspace ../../input/original/SMWW/SMWW_workspace_mu_ww_noEFT_asimov.root w  --output ../../input/edited/SMWW/setup_SMWW_workspace_mu_ww_Asimov.root

These workspaces can now be used to do fitting with the SMWW only workspace. For the combination we need to run it standalone through the RoofitUtils combiner.

    python share/combine.py --input comb_SMWW/combine_SMWWonly_observed.cfg --correlations comb_SMWW/NOcorrelations.txt --output ../../input/edited/SMWW/setup_SMWW_workspace_mu_ww_Observed_Combiner.root --poi "mu_ww"

Again the same for the Asimov only workspace

    python share/combine.py --input comb_SMWW/combine_SMWWonly_asimov.cfg --correlations comb_SMWW/NOcorrelations.txt --output ../../input/edited/SMWW/setup_SMWW_workspace_mu_ww_Asimov_Combiner.root --poi "mu_ww"

These "Combiner" workspace are used for the combination with HWW

    python share/combine.py --input comb_SMWW/combine_HWW_SMWW_observed.cfg --correlations comb_SMWW/correlations-HWW-SMWW-nostxs.txt --output ../../share/workspaces/HWWSMWW-c16a/workspace-preFit-observed.root --poi "mu_GGF" "mu_VBF"

Again the same for the Asimov only workspace

    python share/combine.py --input comb_SMWW/combine_SMWWonly_asimov.cfg --correlations comb_SMWW/NOcorrelations.txt --output ../../input/edited/SMWW/setup_SMWW_workspace_mu_ww_Asimov_Combiner.root --poi "mu_ww"

Now run the HWW combination using:

    python share/combine.py --input comb_SMWW/combine_HWW_SMWW_asimov.cfg --correlations comb_SMWW/correlations-HWW-SMWW-nostxs.txt --output ../../input/edited/SMWW/HWWSMWW_Combined_Asimov_NONP.root --poi "mu_GGF" "mu_VBF"

    python share/combine.py --input comb_SMWW/combine_HWW_SMWW_observed.cfg --correlations comb_SMWW/correlations-HWW-SMWW-nostxs.txt --output ../../input/edited/SMWW/HWWSMWW_Combined_Observed_NONP.root --poi "mu_GGF" "mu_VBF"

Rename and add the Nuisance parameters to the modelconfig used by SFramework and output the combined workspace to the fitting directory

    python scripts/setSMWWNP.py --workspace ../../input/edited/SMWW/HWWSMWW_Combined_Asimov_NONP.root combined --output ../../share/workspaces/HWWSMWW-c16a/workspace-preFit-asimov.root

    python scripts/setSMWWNP.py --workspace ../../input/edited/SMWW/HWWSMWW_Combined_Observed_NONP.root combined --output ../../share/workspaces/HWWSMWW-c16a/workspace-preFit-observed.root
